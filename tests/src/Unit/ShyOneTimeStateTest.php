<?php

declare(strict_types=1);

namespace Drupal\Tests\shy_one_time\Unit;

use Drupal\Core\State\StateInterface;
use Drupal\shy_one_time\ShyOneTimeState;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the Shy One-Time state service.
 *
 * @group shy_one_time
 *
 * @coversDefaultClass \Drupal\shy_one_time\ShyOneTimeState
 */
class ShyOneTimeStateTest extends UnitTestCase {

  /**
   * Date provider for user agent tests.
   *
   * @return array
   *   Outputs an array of user agents.
   */
  public static function userAgentsProvider(): array {

    return [
      ['Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0'],
      ['Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'],
    ];

  }

  /**
   * Test that an empty state value doesn't match non-crawler user agents.
   *
   * @dataProvider userAgentsProvider
   */
  public function testCheckUserAgentsEmptyValue(string $userAgent): void {

    /** @var \Drupal\Core\State\StateInterface Mocked up state manager. */
    $stateManager = $this->prophesize(StateInterface::class);

    $stateManager->get('shy_one_time.user_agents', '')->willReturn('');

    $shyOneTimeState = new ShyOneTimeState($stateManager->reveal());

    $this->assertFalse(
      $shyOneTimeState->checkUserAgents($userAgent),
      'ShyOneTimeState::checkUserAgents() did not return false as expected!'
    );

    $this->assertEquals([], $shyOneTimeState->getUserAgents(),
      'ShyOneTimeState::getUserAgents() did not return an empty array as expected!'
    );

  }

  /**
   * Test that non-empty state value doesn't match non-crawler user agents.
   *
   * @dataProvider userAgentsProvider
   */
  public function testCheckUserAgentsNonEmptyValue(string $userAgent): void {

    /** @var \Drupal\Core\State\StateInterface Mocked up state manager. */
    $stateManager = $this->prophesize(StateInterface::class);

    $stateUserAgents = [
      'baby-shark-do-do-do-do', 'nyan-cat', 'bears-bears-bears',
    ];

    $stateManager->get('shy_one_time.user_agents', '')->willReturn(\implode(
      \PHP_EOL, $stateUserAgents
    ));

    $shyOneTimeState = new ShyOneTimeState($stateManager->reveal());

    $this->assertFalse(
      $shyOneTimeState->checkUserAgents($userAgent),
      'ShyOneTimeState::checkUserAgents() did not return false as expected!'
    );

    foreach ($stateUserAgents as $stateUserAgent) {

      $this->assertTrue(
        $shyOneTimeState->checkUserAgents($stateUserAgent),
        'ShyOneTimeState::checkUserAgents() did not return true as expected!'
      );

    }

    $this->assertEquals($stateUserAgents, $shyOneTimeState->getUserAgents(),
      'ShyOneTimeState::getUserAgents() did not return the expected user agents array!'
    );

  }

}
