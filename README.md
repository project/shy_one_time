CONTENTS OF THIS FILE
---------------------

* Introduction
* Note
* Requirements
* Installation
* Configuration
* Further information on the problem
* Maintainers

# INTRODUCTION

When requesting a one-time login link (**request new password** or **password
forgotten** function), it often comes to the fact that the link arrives
invalidated/invalid via e-mail. This can be observed especially with
applications from Microsoft, e.g. Outlook or Bing, but also with Gmail (possibly
other services). The reason for this is that the link is crawled in advance by
security tools before it is delivered via email. Malicious bots, crawlers or
spiders can cause this problem in the same way.

The result is the following message, which is certainly familiar to some:

> You have tried to use a one-time login link that has either been used or is no
> longer valid. Please request a new one using the form below.

This module prevents crawling of the One-Time Login link.

> This module uses 'CrawlerDetect', a PHP class for detecting
> bots/crawlers/spiders via the `user agent` and `http_from` header. Currently
> able to detect 1,000's of
> bots/spiders/crawlers, [further information](https://crawlerdetect.io).

## Note

It especially affects modules that offer login by email only, e.g.:

> * [Passwordless](https://www.drupal.org/project/passwordless)
> * [Mail Login](https://www.drupal.org/project/mail_login)
> * [Login with Email only](https://www.drupal.org/project/login_onlyemail)

If these modules are used, a single valid login link is sent, this can be
invalidated and thus a login into the system is not possible.

In a pure Drupal installation without additional modules that do not change the
behavior of the login, `Shy One-Time` is to be used only if necessary.

## Requirements

This module requires no modules outside of Drupal core, if the installation is
performed via Composer.

# INSTALLATION

Install the `Shy One-Time` module as you would normally install a contributed
Drupal module via Composer,
[further information](https://www.drupal.org/node/1897420).

### Enable module

Activate the module via the Drupal backend UI or alternatively with Drush:

```
drush en shy_one_time -y
```

## Installation without Composer

Download the module and unpack it in the module folder. For the module to work
you need to download an external library and implement it in Drupal. You can
find it here: https://github.com/JayBizzle/Crawler-Detect. Always use the latest
release.

> Get the latest information for installing an external library here:
> [Installing an external library that is required by a contributed module](https://www.drupal.org/node/2840868)

## Configuration

The module works out of the box, if no individual user agents are entered, only
CrawlerDetect library checks whether the access comes from a bot/crawler.

After installing the module, the configuration interface can be reached via the
link `/admin/config/system/shy_one_time`. User agents that are unwanted and
should be blocked are entered in the text field. Only ONE user agent may be
inserted per line.

The format for custom user agents looks as follows, e.g.

```
Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us; rv:1.9.2.3) Gecko/20100401 YFF35
Firefox/3.6.3
Mozilla/5.0 Windows NT 10.0; Win64; x64 AppleWebKit/537.36 KHTML, like Gecko
Chrome/65.0.3286.0 Safari/537.36 Rigor
```

### Note

From now on, all requests made via the route `user.reset` will be logged in the
dblog. If a login does not work, it is possible to quickly check which user
agent is involved and transfer it to the check routine.

### Further information on the problem

* (d.o
  issue) [Bingpreview invalidates one time login links](https://www.drupal.org/project/drupal/issues/2828034)
* [Safe Links in Microsoft Defender for Office 365](https://docs.microsoft.com/en-us/microsoft-365/security/office-365-security/safe-links?view=o365-worldwide#do-not-rewrite-the-following-urls-lists-in-safe-links-policies)

MAINTAINERS
-----------

Supporting organization:

* [TRENDKRAFT](https://www.drupal.org/trendkraft)
