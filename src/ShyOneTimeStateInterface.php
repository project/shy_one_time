<?php

namespace Drupal\shy_one_time;

/**
 * Interface for the Shy One Time state service.
 */
interface ShyOneTimeStateInterface {

  /**
   * Gets the value of the 'shy_one_time.user_agents' state variable.
   *
   * @return string
   *   The value of the 'shy_one_time.user_agents' state variable.
   */
  public function getUserAgents();

  /**
   * Gets the value of the 'shy_one_time.user_agents' state variable.
   *
   * @return string
   *   The value of the 'shy_one_time.user_agents' state variable.
   */
  public function checkUserAgents($currentUserAgent);

}
