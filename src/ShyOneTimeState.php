<?php

namespace Drupal\shy_one_time;

use Drupal\Core\State\StateInterface;

/**
 * Provides a service for the Shy One Time state variables.
 */
class ShyOneTimeState implements ShyOneTimeStateInterface {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new ShyOneTimeState object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserAgents(): array|string {

    $userAgents = $this->state->get('shy_one_time.user_agents', '');

    // Return an empty array if the value is empty to avoid explode() returning
    // an array with one empty string value.
    if (empty($userAgents)) {
      return [];
    }

    return explode(PHP_EOL, rtrim($userAgents));

  }

  /**
   * {@inheritdoc}
   */
  public function checkUserAgents($currentUserAgent): bool|string {

    $isBlockedUserAgent = FALSE;

    // Skip processing when user agent is empty. This has been encountered
    // lately that systems such as Microsoft Office Crawler simply do not
    // pass valid or empty user agents.
    if (empty($currentUserAgent)
        || (in_array($currentUserAgent, ['-', '--', '---']))) {
      return TRUE;
    }

    foreach ($this->getUserAgents() as $userAgent) {

      $trimmedUserAgent = preg_replace('/\s+/', ' ', trim($userAgent));

      // Skip empty user agent strings to avoid false positives. This can happen
      // when the state value is an empty string.
      if (empty($trimmedUserAgent)) {
        continue;
      }

      $patternUserAgent = "/" . preg_quote($trimmedUserAgent, '/') . "/i";

      if (preg_match($patternUserAgent, $currentUserAgent)) {
        $isBlockedUserAgent = TRUE;
        break;
      }
    }

    return $isBlockedUserAgent;
  }

}
