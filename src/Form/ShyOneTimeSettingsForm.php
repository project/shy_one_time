<?php

namespace Drupal\shy_one_time\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The ShyOneTimeSettingsForm class provides a form for module settings.
 */
class ShyOneTimeSettingsForm extends FormBase {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a ShyOneTimeSettingsForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'shy_one_time_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user_agents = $this->state->get('shy_one_time.user_agents', '');

    $form['user_agents'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Individual user agents that are blocked.'),
      '#description' => $this->t('Insert your user agents here, which will be checked and locked out. Always insert only one user agent per line. If you are not sure what a user agent looks like, you can copy the user agent you want to block from the dblog. Be careful that you have selected the CORRECT user agent, otherwise the user will not be able to log in.'),
      '#default_value' => $user_agents,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set('shy_one_time.user_agents', $form_state->getValue('user_agents'));
    $this->messenger()
      ->addMessage($this->t('The configuration options have been saved.'));
  }

}
