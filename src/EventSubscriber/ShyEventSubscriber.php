<?php

namespace Drupal\shy_one_time\EventSubscriber;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\shy_one_time\ShyOneTimeStateInterface;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Subscriber checks if the reset form is accessed by a bot/crawler.
 */
class ShyEventSubscriber implements EventSubscriberInterface, ContainerInjectionInterface {

  /**
   * The Shy One Time state service.
   *
   * @var \Drupal\shy_one_time\ShyOneTimeStateInterface
   */
  protected ShyOneTimeStateInterface $shyOneTimeState;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * Constructs a new ShyOneTimeEventSubscriber object.
   *
   * @param \Drupal\shy_one_time\ShyOneTimeStateInterface $shy_one_time_state
   *   The Shy One Time state service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(
    ShyOneTimeStateInterface $shy_one_time_state,
    LoggerChannelFactoryInterface $logger_factory,
  ) {
    $this->shyOneTimeState = $shy_one_time_state;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('shy_one_time.state'),
      $container->get('logger.factory')
    );
  }

  /**
   * React to a kernel request event.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event object.
   */
  public function shyOneTimeRequest(RequestEvent $event) {

    if (!$event->isMainRequest()) {
      return;
    }

    $request = $event->getRequest();

    // Only check for the specific internal route 'user.reset'.
    if ($request->attributes->get('_route') === 'user.reset') {

      // Check if the request is from a Crawler/Bot or custom user agents.
      if ((new CrawlerDetect())->isCrawler()) {
        throw new AccessDeniedHttpException();
      }
      elseif ($this->shyOneTimeState->checkUserAgents(
        $event->getRequest()->headers->get('User-Agent'))) {
        $redirectUrl = Url::fromRoute('user.login')->toString();
        // Create a redirect response to the special node.
        $response = new RedirectResponse($redirectUrl);
        // Set the redirect status code (optional).
        $response->setStatusCode(302);
        // Set any additional headers (optional).
        $response->headers->set('Cache-Control', 'no-cache');
        // Return the redirect response to stop further processing.
        $event->setResponse($response);
      }

      // Log the user agent access.
      $current_user_agent = $event->getRequest()->headers->get('User-Agent');
      if ((!empty($current_user_agent))) {
        $this->loggerFactory->get('shy_one_time')
          ->notice('User-Agent | ' . $current_user_agent, ['@user_agent' => $current_user_agent]);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[KernelEvents::REQUEST][] = ['shyOneTimeRequest'];
    return $events;
  }

}
